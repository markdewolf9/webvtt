// parse timestamp string into double
function parse_timestamp(s) {
    var match = s.match(/^(?:([0-9]+):)?([0-5][0-9]):([0-5][0-9](?:[.,][0-9]{0,3})?)/);
    if (match == null) {
        throw 'Invalid timestamp format: ' + s;
    }
    var hours = parseInt(match[1] || "0", 10);
    var minutes = parseInt(match[2], 10);
    var seconds = parseFloat(match[3].replace(',', '.'));
    return seconds + 60 * minutes + 60 * 60 * hours;
}

// parse WebVTT file into list of VTTCue objects
// (partly based on code found at https://bl.ocks.org/denilsonsa/aeb06c662cf98e29c379)
function parse_vtt(vtt) {
    var lines = vtt.trim().replace('\r\n', '\n').split(/[\r\n]/).map(function(line) {
        return line.trim();
    });
    var cues = [];
    var start = null;
    var end = null;
    var payload = null;
    for (var i = 0; i < lines.length; i++) {
        if (lines[i].indexOf('-->') >= 0) {
            var splitted = lines[i].split(/[ \t]+-->[ \t]+/);
            if (splitted.length != 2) {
                throw 'Error when splitting "-->": ' + lines[i];
            }
            // Already ignoring anything past the "end" timestamp (i.e. cue settings).
            start = parse_timestamp(splitted[0]);
            end = parse_timestamp(splitted[1]);
        } else if (lines[i] == '') {
            if (start && end) {
                var cue = new VTTCue(start, end, payload);
                cues.push(cue);
                start = null;
                end = null;
                payload = null;
            }
        } else if(start && end) {
            if (payload == null) {
                payload = lines[i];
            } else {
                payload += '\n' + lines[i];
            }
        }
    }
    if (start && end) {
        var cue = new VTTCue(start, end, payload);
        cues.push(cue);
    }
    return cues;
}

// Binary search to find the active cue, based on the currentTime
function getActiveCue(cues, currentTime) {
    if (cues.length == 0) {return []};
    var L = 0;
    var R = cues.length-1;
    var m = Math.floor(cues.length/2);
    result = [];
    if (cues[L].startTime > currentTime || cues[R].endTime < currentTime) {
        // currentTime is outside subtitle scope
        lastActiveCueIndex = -1;
    }
    while (L <= R) {
        m = Math.floor((L + R)/2);
        if (cues[L].startTime > currentTime || cues[R].endTime < currentTime) {
            // currentTime is not during a cue
            result = [];
            break;
        } else if (cues[m].endTime < currentTime) {
            L = m + 1;
        } else if (cues[m].startTime > currentTime) {
            R = m - 1;
        } else {
            result = [cues[m]];
            lastActiveCueIndex = m;
            break;
        }
    }
    return result;
}

// returns the cue to show on a time update event
function updateActiveCue(cues, currentTime) {
    if (lastActiveCueIndex >= 0 && cues[lastActiveCueIndex].endTime >= currentTime &&
        cues[lastActiveCueIndex].startTime <= currentTime) {
        // return current cue
        return [cues[lastActiveCueIndex]];
    } else if (lastActiveCueIndex >= 0 && cues[lastActiveCueIndex+1] &&
        cues[lastActiveCueIndex+1].startTime <= currentTime) {
        // return next cue relative to lastActiveCue
        lastActiveCueIndex = lastActiveCueIndex + 1;
        return [cues[lastActiveCueIndex]];
    } else if (lastActiveCueIndex >= 0 && cues[lastActiveCueIndex].endTime < currentTime) {
        // return no cue, since currentTime is between cues
        return [];
    } else {
        // in case no lastActiveCue: search until active cue found
        return getActiveCue(cues, currentTime);
    }
}

// parse and show active cues in html
function downloadFinished(subtitles) {
    // parse subtitles to list of individual cues
    cues = parse_vtt(subtitles);
    // get the video element by id
    var vid = document.getElementById("video-example");
    // create array for activeCue
    activeCue = getActiveCue(cues, vid.currentTime);
    // onseeked event: binary search for current active cue
    vid.onseeked = function() {
        activeCue = getActiveCue(cues, vid.currentTime);
        document.getElementById("currentTime").innerHTML = vid.currentTime;
    };
    // ontimeupdate event: update cue based on last active cue, and update HTML elements
    vid.ontimeupdate = function() {
        activeCue = updateActiveCue(cues, vid.currentTime);
        document.getElementById("currentTime").innerHTML = vid.currentTime;
        if (activeCue.length != 0) {
            document.getElementById("currentSubtitle").innerHTML = activeCue[0].text;
        } else {
            document.getElementById("currentSubtitle").innerHTML = "";
        }
    };
}

// download external WebVTT file from url
function downloadFile(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {callback(xhr.responseText)};
    };
    xhr.open("GET", url, true);
    xhr.send();
}

// initialise parameter
var lastActiveCueIndex = -1;
// starting function
downloadFile(document.getElementById("video-example-track").src, downloadFinished);
