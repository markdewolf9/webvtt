## Introduction

This project implements a very basic WebVTT parser and renderer.

More specifically, a WebVTT subtitles file is downloaded, parsed into VTTCue objects, and rendered when the `<video>` element is being played, using standard JavaScript.

*NOTE: this project is the developer's first experience with standard JavaScript (without framework) and the WebVTT specification, written within a limited amount of time.*


## Instructions

To test this project, simply open the index.html file in a web browser. Once the user starts the video, the currentTime and currentSubtitle will be shown underneath the video view.

The video URL and WebVTT are hardcoded in the index.html file at this moment, for the open-source Sintel movie project. These can easily be changed to other URLs to test other movies, together with their WebVTT subtitle file.

*Currently, Safari 11.1.1 on MacOS 10.13.5 and Chrome 68 on MacOS 10.13.5 are tested.*


## Changes in implementation

**September 13th**
* The search algorithm to find the current cue (after a `seeked` event) has been changed to a modified binary search algorithm. This algorithm assumes no two subtitle cues overlap. Otherwise, only one is marked as active.
* On a `timeUpdate` event, the active cue is selected based on the last active cue and the currentTime property. This technique requires no search through the complete collection of subtitle cues, but only needs to verify the timestamps of the last active cue and the next one, if necessary.
